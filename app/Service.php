<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    public function car()
  {
      return $this->belongsTo('App\Car');
  }

  public function driver()
{
    return $this->belongsTo('App\Cms_user');
}

}
