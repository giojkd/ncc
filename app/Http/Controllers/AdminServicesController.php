<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use PDF;

class AdminServicesController extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {


		$cms_users_id = CRUDBooster::myId();
		$user = DB::table('cms_users')->where('id',$cms_users_id)->first();
		$ut = $user->id_cms_privileges;
		$day_progressive_ids = implode([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],';');


		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "passenger_name";
		$this->limit = "20";
		$this->orderby = "starts_at,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = true;
		$this->table = "services";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"ID","name"=>"id"];
		$this->col[] = ["label"=>"PG","name"=>"day_progressive_id"];
		$this->col[] = ["label"=>"Cliente","name"=>"client_id","join"=>"clients,company"];
		#$this->col[] = ["label"=>"Autista","name"=>"(SELECT CONCAT(name,' ',surname,' (',company,')') FROM cms_users WHERE cms_users.id = services.driver_id) as driver"];
		$this->col[] = ["label"=>"Autista","name"=>"driver_id","join"=>"cms_users,company"];

		$this->col[] = ["label"=>"Inizio servizio","name"=>"starts_at",'callback_php'=>'date("d/m/Y H:i",strtotime($row->starts_at))'];
		$this->col[] = ["label"=>"Da","name"=>"from"];
		$this->col[] = ["label"=>"A","name"=>"to"];
		$this->col[] = ["label"=>"Tipo","name"=>"type"];
		$this->col[] = ["label"=>"Status","name"=>"status"];
		$this->col[] = ["label"=>"Descrizione","name"=>"description"];
		$this->col[] = ["label"=>"Passeggero","name"=>"passenger_name"];
		$this->col[] = ["label"=>"Costo autista","name"=>"driver_cost"];
		$this->col[] = ["label"=>"Prezzo al cliente","name"=>"price"];
		#$this->col[] = ["label"=>"Prezzo al cliente","name"=>"price"];
		#$this->col[] = ["label"=>"Costo autista","name"=>"driver_cost"];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Progressivo giornaliero','name'=>'day_progressive_id','type'=>'select','validation'=>'min:1|max:255','width'=>'col-sm-10','dataenum'=>$day_progressive_ids];
		if($ut==1)
		$this->form[] = ['label'=>'Cliente','name'=>'client_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'clients,company'];

		$this->form[] = ['label'=>'Inizio','name'=>'starts_at','type'=>'datetime','width'=>'col-sm-10','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Fine','name'=>'ends_at','type'=>'datetime','width'=>'col-sm-10','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Tipo','name'=>'type','type'=>'select2','validation'=>'min:1|max:255','width'=>'col-sm-10','dataenum'=>'Transfer;Disposizione;Tour;Altro;Navetta;Evento;Transfer più stop','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Riferimento','name'=>'passenger_reference','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','placeholder'=>'Numero volo, orario treno, etc...','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Descrizione','name'=>'description','type'=>'textarea','validation'=>'string|min:5|max:5000','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Da','name'=>'from','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'A','name'=>'to','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Tipo Auto','name'=>'car_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'cars,name','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Numero di passeggeri','name'=>'passengers_number','type'=>'number','validation'=>'min:0','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Nome Passeggero','name'=>'passenger_name','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Cellulare del passeggero','name'=>'passenger_mobile','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','disabled'=>($ut==2) ? true : false];

		if($ut==1)
		$this->form[] = ['label'=>'Prezzo al Cliente','name'=>'price','type'=>'text','validation'=>'min:0','width'=>'col-sm-10'];

		$this->form[] = ['label'=>'Metodo di pagamento','name'=>'payment_method','type'=>'select2','validation'=>'min:1|max:255','width'=>'col-sm-10','dataenum'=>'Cash;Bonifico;CC','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Incassa l\'autista?','name'=>'driver_collected','type'=>'radio','validation'=>'integer|min:0','width'=>'col-sm-10','dataenum'=>'1|Sì;0|No','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Ammontare incassato','name'=>'collected_amount','type'=>'text','validation'=>'min:0','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Note','name'=>'notes','type'=>'textarea','validation'=>'string|min:5|max:5000','width'=>'col-sm-10','disabled'=>($ut==2) ? true : false];
		if($ut==1){
			$statuses = "Accettato;Rifiutato;Eseguito;Cancellato";
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'select','validation'=>'min:1|max:255','width'=>'col-sm-10','dataenum'=>$statuses,'default'=>'In sospeso'];
		}
		if($ut==2){
			$statuses = "Accettato;Rifiutato;Eseguito";
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'select','validation'=>'min:1|max:255','width'=>'col-sm-10','dataenum'=>$statuses];
		}

		if($ut==1)
		$this->form[] = ['label'=>'Autista','name'=>'driver_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,user_label'];
		$this->form[] = ['label'=>'Costo autista','name'=>'driver_cost','type'=>'text','validation'=>'min:0','width'=>'col-sm-10','disabled'=>($ut==2) ? true : false];
		$this->form[] = ['label'=>'Km all\'inizio','name'=>'km_when_started','type'=>'number','validation'=>'integer|min:0','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Km alla fine','name'=>'km_when_ended','type'=>'number','validation'=>'integer|min:0','width'=>'col-sm-10'];
		#$this->form[] = ['label'=>'Iniziato il','name'=>'started_at','type'=>'datetime','width'=>'col-sm-10'];
		#$this->form[] = ['label'=>'Finito il','name'=>'ended_at','type'=>'datetime','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Note autista','name'=>'driver_notes','type'=>'textarea','validation'=>'string|min:5|max:5000','width'=>'col-sm-10'];

		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ["label"=>"Client Id","name"=>"client_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"client,id"];
		//$this->form[] = ["label"=>"Price","name"=>"price","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
		//$this->form[] = ["label"=>"Payment Method","name"=>"payment_method","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Driver Collected","name"=>"driver_collected","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
		//$this->form[] = ["label"=>"Collected Amount","name"=>"collected_amount","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
		//$this->form[] = ["label"=>"Car Id","name"=>"car_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"car,id"];
		//$this->form[] = ["label"=>"Starts At","name"=>"starts_at","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
		//$this->form[] = ["label"=>"Ends At","name"=>"ends_at","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
		//$this->form[] = ["label"=>"Type","name"=>"type","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"From","name"=>"from","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"To","name"=>"to","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Notes","name"=>"notes","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
		//$this->form[] = ["label"=>"Passenger Name","name"=>"passenger_name","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Passenger Mobile","name"=>"passenger_mobile","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Passenger Reference","name"=>"passenger_reference","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Driver Id","name"=>"driver_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"driver,id"];
		//$this->form[] = ["label"=>"Driver Cost","name"=>"driver_cost","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
		//$this->form[] = ["label"=>"Driver Notes","name"=>"driver_notes","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
		//$this->form[] = ["label"=>"Km When Started","name"=>"km_when_started","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
		//$this->form[] = ["label"=>"Km When Ended","name"=>"km_when_ended","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
		//$this->form[] = ["label"=>"Started At","name"=>"started_at","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
		//$this->form[] = ["label"=>"Ended At","name"=>"ended_at","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
		# OLD END FORM

		/*
		| ----------------------------------------------------------------------
		| Sub Module
		| ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
		|
		*/
		$this->sub_module = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Action Button / Menu
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
		| @icon        = Font awesome class icon. e.g : fa fa-bars
		| @color 	   = Default is primary. (primary, warning, succecss, info)
		| @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
		|
		*/
		$this->addaction = array();
		$this->addaction[] = ['label'=>'Invia email','icon'=>'fa fa-envelope','color'=>'warning','url'=>CRUDBooster::mainpath('send-service-request-confirmation-email').'/[id]'];
		$this->addaction[] = ['label'=>'Stampa Form','icon'=>'fa fa-print','color'=>'warning','url'=>CRUDBooster::mainpath('print-service-form').'/[id]'];
		$this->addaction[] = ['label'=>'Stampa Banner','icon'=>'fa fa-print','color'=>'warning','url'=>CRUDBooster::mainpath('print-service-banner').'/[id]'];


		/*
		| ----------------------------------------------------------------------
		| Add More Button Selected
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @icon 	   = Icon from fontawesome
		| @name 	   = Name of button
		| Then about the action, you should code at actionButtonSelected method
		|
		*/
		$this->button_selected = array();


		/*
		| ----------------------------------------------------------------------
		| Add alert message to this module at overheader
		| ----------------------------------------------------------------------
		| @message = Text of message
		| @type    = warning,success,danger,info
		|
		*/
		$this->alert        = array();



		/*
		| ----------------------------------------------------------------------
		| Add more button to header button
		| ----------------------------------------------------------------------
		| @label = Name of button
		| @url   = URL Target
		| @icon  = Icon from Awesome.
		|
		*/
		$this->index_button = array();




		/*
		| ----------------------------------------------------------------------
		| Customize Table Row Color
		| ----------------------------------------------------------------------
		| @condition = If condition. You may use field alias. E.g : [id] == 1
		| @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
		|
		*/
		$this->table_row_color = array();


		/*
		| ----------------------------------------------------------------------
		| You may use this bellow array to add statistic at dashboard
		| ----------------------------------------------------------------------
		| @label, @count, @icon, @color
		|
		*/
		$this->index_statistic = array();



		/*
		| ----------------------------------------------------------------------
		| Add javascript at body
		| ----------------------------------------------------------------------
		| javascript code in the variable
		| $this->script_js = "function() { ... }";
		|
		*/
		$this->script_js = NULL;


		/*
		| ----------------------------------------------------------------------
		| Include HTML Code before index table
		| ----------------------------------------------------------------------
		| html code to display it before index table
		| $this->pre_index_html = "<p>test</p>";
		|
		*/



		ob_start();
		/*echo '<pre>';
		print_r($_GET);
		echo '</pre>';*/
		$clients= DB::table('clients')->get();
		$drivers = DB::table('cms_users')->where('id_cms_privileges',2)->get();#only drivers
		$_GET['filter_column']['services.starts_at']['value'][0] = ($_GET['filter_column']['services.starts_at']['value'][0]) ? : date('Y-m-d');
		$_GET['filter_column']['services.starts_at']['value'][1] = ($_GET['filter_column']['services.starts_at']['value'][1]) ? : date('Y-m-d',strtotime('+7days'));
		?>
		<form class="form form-inline" method="/admin/matches">
			<input type="hidden" name="filter_column[starts_at][type]" value="between">
			<input type="hidden" name="filter_column[clients.company][type]" value="like">
			<input type="hidden" name="filter_column[cms_users.company][type]" value="like">
			<div class="form-group">
				<label value="">Cliente:</label>
				<!--filter_column[clients.company][value]-->
				<select name="filter_column[clients.company][value]" id="" class="form-control">
					<option value="">Scegli...</option>
					<?php foreach($clients as $client){?>
						<option <?php echo ($_GET['filter_column']['clients.company']['value']==$client->company) ? 'selected' : ''?> value="<?php echo $client->company?>"><?php echo $client->company?></option>
					<?php }?>
				</select>
			</div>
			<div class="form-group">
				<label>Autista:</label>
				<!--filter_column[cms_users.id][value]-->
				<select name="filter_column[cms_users.company][value]" id="" class="form-control" >
					<option value="">Scegli...</option>
					<?php foreach($drivers as $driver){?>
						<option <?php echo ($_GET['filter_column']['cms_users.company']['value']==$driver->company) ? 'selected' : ''?> value="<?php echo $driver->company?>"><?php echo $driver->company?></option>
					<?php }?>
				</select>
			</div>

			<div class="form-group">
				<label>Da:</label>
				<input value="<?php echo($_GET['filter_column']['starts_at']['value'][0]) ?>" name="filter_column[starts_at][value][0]" type="text" class="form-control datepicker">
			</div>
			<div class="form-group">
				<label>A:</label>
				<input value="<?php echo($_GET['filter_column']['starts_at']['value'][1]) ?>" name="filter_column[starts_at][value][1]" type="text" class="form-control datepicker">
			</div>

			<button style="margin-top:24px" type="submit" class="btn btn-sm btn-success">Filtra</button>
		</form>
		<?php
		$cane = ob_get_clean();
		$this->pre_index_html = $cane;





		/*
		| ----------------------------------------------------------------------
		| Include HTML Code after index table
		| ----------------------------------------------------------------------
		| html code to display it after index table
		| $this->post_index_html = "<p>test</p>";
		|
		*/



		$this->post_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include Javascript File
		| ----------------------------------------------------------------------
		| URL of your javascript each array
		| $this->load_js[] = asset("myfile.js");
		|
		*/
		$this->load_js = array();
		$this->load_js[] = asset("/js/index.js");


		/*
		| ----------------------------------------------------------------------
		| Add css style at body
		| ----------------------------------------------------------------------
		| css code in the variable
		| $this->style_css = ".style{....}";
		|
		*/
		$this->style_css = NULL;




		/*
		| ----------------------------------------------------------------------
		| Include css File
		| ----------------------------------------------------------------------
		| URL of your css each array
		| $this->load_css[] = asset("myfile.css");
		|
		*/
		$this->load_css = array();
		$this->load_css[] = asset("/css/index.css");

	}

	public function getPrintServiceBanner($id){
		$service = \App\Service::with(['car','driver'])->find($id);
		$data = [
			'passenger_name' => $service->passenger_name
		];
		$pdf = PDF::loadView('serviceBanner',$data);
		return $pdf->setPaper('A4','landscape')->download('service_banner_'.$id.'.pdf');
	}

	public function getPrintServiceForm($id) {
		$service = \App\Service::with(['car','driver'])->find($id);


		/*
		return view('mail_test_1',[
		'team_name' => $team_host->team_name,
		'match_label' => $match->match_label,
		'tournament_name' => $tournament->tournament_name,
		'match_day' => $match->match_day,
		'field_name'=> $match->field_name,
		'round_label'=> $tournament->round_label,
		'time_frame_start' => $match->time_frame_start,
		'players' => $team_host_players
	]);
	exit;*/


	$data = [
		'day' => date ('d/m/Y',strtotime($service->starts_at)),
		'passenger_name' => $service->passenger_name,
		'time' => date('H:i',strtotime($service->starts_at)),
		'km_begin' => $service->km_when_started,
		'pickup_place' => $service->from,
		'car_type' => $service->car->type,
		'car_plate' => $service->car->plate,
		'driver_name' => $service->driver->name.' '.$service->driver->surname.' ('.$service->driver->company.')',
		'delivery_place' => $service->to,
		'service_type' => $service->type,
		'day_progressive' => $service->day_progressive_id,
		'notes' => $service->notes

	];


	$pdf = PDF::loadView('serviceForm',$data);
	return $pdf->setPaper('A4')->download('service_form_'.$id.'.pdf');
}


/*
| ----------------------------------------------------------------------
| Hook for button selected
| ----------------------------------------------------------------------
| @id_selected = the id selected
| @button_name = the name of button
|
*/
public function actionButtonSelected($id_selected,$button_name) {
	//Your code here

}


/*
| ----------------------------------------------------------------------
| Hook for manipulate query of index result
| ----------------------------------------------------------------------
| @query = current sql query
|
*/
public function hook_query_index(&$query) {
	//Your code here
	#$query->addSelect(DB::raw("(SELECT CONCAT(name,surname) FROM cms_users WHERE cms_users.id = services.driver_id) as driver"));

}

/*
| ----------------------------------------------------------------------
| Hook for manipulate row of index table html
| ----------------------------------------------------------------------
|
*/
public function hook_row_index($column_index,&$column_value) {
	//Your code here
}

/*
| ----------------------------------------------------------------------
| Hook for manipulate data input before add data is execute
| ----------------------------------------------------------------------
| @arr
|
*/

public function getSendServiceRequestConfirmationEmail($id){

	$service = \App\Service::find($id);



	$driver = DB::table('cms_users')->where('id',$service->driver_id)->first();
	$car = DB::table('cars')->where('id',$service->car_id)->first();

	$data = [
		'drivers_name'=>$driver->name,
		'service_starts_at'=>date('d/m/Y H:i',strtotime($service->starts_at)),
		'service_ends_at'=>(!is_null($service->ends_at)) ? date('d/m/Y H:i',strtotime($service->ends_at)) : '', 
		'service_from'=>$service->from,
		'service_to'=>$service->to,
		'car_name'=>$car->name,
		'service_type'=>$service->type,
		'description' => $service->description,
		'passenger_reference' => $service->passenger_reference,
		'service_id' => $service->id
	];



	CRUDBooster::sendEmail(['to'=>$driver->email,'data'=>$data,'template'=>'driver_notification']);
	CRUDBooster::redirectBack('Email inviata','success');
}

public function hook_before_add(&$postdata) {
	//Your code here


}

/*
| ----------------------------------------------------------------------
| Hook for execute command after add public static function called
| ----------------------------------------------------------------------
| @id = last insert id
|
*/
public function hook_after_add($id) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for manipulate data input before update data is execute
| ----------------------------------------------------------------------
| @postdata = input post data
| @id       = current id
|
*/
public function hook_before_edit(&$postdata,$id) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for execute command after edit public static function called
| ----------------------------------------------------------------------
| @id       = current id
|
*/
public function hook_after_edit($id) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for execute command before delete public static function called
| ----------------------------------------------------------------------
| @id       = current id
|
*/
public function hook_before_delete($id) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for execute command after delete public static function called
| ----------------------------------------------------------------------
| @id       = current id
|
*/
public function hook_after_delete($id) {
	//Your code here

}



//By the way, you can still create your own method in here... :)


}
