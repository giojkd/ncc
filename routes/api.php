<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('ssu/{id}/{status}/',function($id,$status){
  $statusMap = [0=>'Rifiutato',1=>'Accettato'];
  $statusMapAlertClass = [0=>'Danger',1=>'Success'];
  $service = \App\Service::findOrFail($id);
  $service->status = $statusMap[$status];
  $service->save();

  $driver = DB::table('cms_users')->where('id',$service->driver_id)->first();
  $data = [
    'drivers_name'=>$driver->name,
    'service_starts_at'=>date('d/m/Y H:i',strtotime($service->starts_at)),
    'service_from'=>$service->from,
    'service_to'=>$service->to,
    'service_type'=>$service->type,
    'service_status' => $statusMap[$status],
  ];

  CRUDBooster::sendEmail(['to'=>'marco.carraresi@inwind.it','data'=>$data,'template'=>'service_acceptation_notification']);

  return view('ssu',['alertClass'=>strtolower($statusMapAlertClass[$status]),'serviceStatus'=>$statusMap[$status]]);
});
