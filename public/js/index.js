  $(function(){
  $('#table_dashboard tbody tr').each(function(){
    var tr = $(this);

    var rowStatus = tr.find('td').eq(9).html();


    switch(rowStatus){
      case 'In attesa di accettazione':
      tr.addClass('ncc-row-iada')
      break;
      case 'In sospeso':
      tr.addClass('success')
      break;
      case 'Accettato':
      tr.addClass('ncc-row-accettato')
      break;
      case 'Rifiutato':
      tr.addClass('warning')
      break;
      case 'Eseguito':
      tr.addClass('active')
      break;
      case 'Cancellato':
      tr.addClass('info')
      break;
    }
  })
})
