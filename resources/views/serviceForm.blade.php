<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">

  <!-- Bootstrap CSS -->


  <title>Hello, world!</title>
  <style media="screen">
  html, body{
    font-family: arial, sans-serif;

  }
  h1{
    font-size: 24px;
    text-transform: uppercase;
    padding:0px;
    margin:0px;
  }
  h2{
    font-size: 20px;
    padding:0px;
    margin:0px;
  }
  h3{
    font-size: 16px;
    text-transform: uppercase;
    padding:0px;
    margin:0px;
  }
  .bordered-box{
    border:1px solid black;
    height: 40px;
    line-height: 32px;
    text-align: center;
  }
  .text-center{
    text-align: center;
  }
  .small{
    font-size: 13px;
    text-transform: uppercase;
    display: block;
    text-align: center;
    margin-top:15px;
  }
  .logo{
    text-align: center
  }
  .logo img{
    height: 100px;
  }
  </style>
</head>
<body>
  <div class="logo">
    <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'/img/logo_marco_carraresi.jpg'?>" alt="">
  </div>

  <div style="height:20px;"></div>
  <table style="width:100%;">
    <tr>
      <td style="width:65%" class="bordered-box text-center" colspan="3">
        <h3>ordine di servizio</h3>
      </td>
      <td style="width:5%"></td>
      <td style="width:30%">
        <span class="small">numero</span>
        <div class="bordered-box">
          {{$day_progressive}}
        </div>
      </td>
    </tr>
    <tr>
      <td style="width:30%">
        <span class="small">data</span>
        <div class="bordered-box">
          {{$day}}
        </div>
      </td>
      <td style="width:5%"></td>

      <td style="width:65%" colspan="3">
        <span class="small">nome ospiti</span>
        <div class="bordered-box">
          {{$passenger_name}}
        </div>
      </td>
    </tr>
    <tr>
      <td style="width:30%">
        <span class="small">ora inizio servizio</span>
        <div class="bordered-box">
          {{$time}}
        </div>
      </td>
      <td style="width:5%"></td>
      <td style="width:30%">
        <span class="small">ora fine servizio</span>
        <div class="bordered-box">

        </div>
      </td>
      <td style="width:5%"></td>
      <td style="width:30%">
        <span class="small">km inizio</span>
        <div class="bordered-box">
          {{$km_begin}}
        </div>
      </td>
    </tr>
    <tr>
      <td style="width:65%" colspan="3">
        <span class="small">luogo pick up</span>
        <div class="bordered-box">
          {{$pickup_place}}
        </div>
      </td>
      <td style="width:5%"></td>
      <td style="width:30%" c>
        <span class="small">km fine</span>
        <div class="bordered-box">

        </div>
      </td>
    </tr>
    <tr>
      <td style="width:100%" colspan="5">
        <span class="small">tipo di servizio</span>
        <div class="bordered-box">
          {{$service_type}}
        </div>
      </td>
    </tr>
    <tr>
      <td style="width:65%" colspan="3">
        <span class="small">veicolo</span>
        <div class="bordered-box">
          {{$car_type}}
        </div>
      </td>
      <td style="width:5%"></td>
      <td style="width:30%">
        <span class="small">targa</span>
        <div class="bordered-box">
          {{$car_plate}}
        </div>
      </td>
    </tr>
    <tr>
      <td style="width:100%" colspan="5">
        <span class="small">autista</span>
        <div class="bordered-box">
          {{$driver_name}}
        </div>
      </td>
    </tr>
    <tr>
      <td style="width:65%" colspan="3">
        <span class="small">luogo fine servizio</span>
        <div class="bordered-box">
          {{$delivery_place}}
        </div>
      </td>
      <td style="width:5%"></td>
      <td style="width:30%" >
        <span class="small">cf autista</span>
        <div class="bordered-box">

        </div>
      </td>
    </tr>
    <tr>
      <td style="width:100%" colspan="5">
        <span class="small">note</span>
        <div class="bordered-box">
          {{$notes}}
        </div>
      </td>
    </tr>
  </table>
  <div style="height:40px;"></div>
  <h3 class="text-center">autonoleggio marco carraresi via frà ruffino 8/i 50125 firenze</h3>
</body>
</html>
