<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
      h1{
        font-size: 160px;
        line-height: 100px;
        text-align: center;
        margin-left: 10px 0px;
      }
      .logo{
        text-align: center
      }
      .logo img{
        height: 140px;
      }
    </style>
  </head>
  <body>
    <div class="logo">
      <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'/img/logo_marco_carraresi.jpg'?>" alt="">
    </div>
      <h1>{{$passenger_name}}</h1>
  </body>
</html>
