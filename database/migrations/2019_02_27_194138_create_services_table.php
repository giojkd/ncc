<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('client_id')->nullable()->index();
            $table->double('price')->nullable();
            $table->string('payment_method',100)->nullable();
            $table->integer('driver_collected')->nullable();
            $table->double('collected_amount')->nullable();
            $table->integer('car_id')->nullable()->index();
            $table->timestamp('starts_at')->nullable();
            $table->timestamp('ends_at')->nullable();
            $table->string('type',100)->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->longText('notes')->nullable();
            $table->string('passenger_name',100)->nullable();
            $table->string('passenger_mobile',100)->nullable();
            $table->string('passenger_reference')->nullable();
            $table->string('status',100)->nullable();
            $table->integer('driver_id')->nullable()->index();
            $table->double('driver_cost')->nullable();
            $table->longText('driver_notes')->nullable();
            $table->double('km_when_started')->nullable();
            $table->double('km_when_ended')->nullable();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('ended_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
