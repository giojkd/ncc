<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name',100)->nullable();
            $table->string('surname',100)->nullable();
            $table->string('company',100)->nullable();
            $table->string('vat_number',100)->nullable();
            $table->string('address',100)->nullable();
            $table->string('zip_code',100)->nullable();
            $table->string('province',100)->nullable();
            $table->string('email',100)->nullable();
            $table->string('telephone',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
